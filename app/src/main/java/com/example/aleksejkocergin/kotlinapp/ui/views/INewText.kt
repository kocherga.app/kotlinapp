package com.example.aleksejkocergin.kotlinapp.ui.views

import com.arellomobile.mvp.MvpView

interface INewText : MvpView {

    fun showMessage(message: String)

    fun hideMessage()

    fun showProgress()

    fun hideProgress()
}