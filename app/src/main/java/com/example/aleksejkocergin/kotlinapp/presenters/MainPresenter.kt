package com.example.aleksejkocergin.kotlinapp.presenters

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.aleksejkocergin.kotlinapp.ui.views.IMain

@InjectViewState
class MainPresenter : MvpPresenter<IMain>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        showNewTextFragment()
    }

    fun showNewTextFragment() {
        viewState.showNewTextFragment()
    }

    fun showHistoryFragment() {
        viewState.showHistoryFragment()
    }
}