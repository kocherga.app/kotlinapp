package com.example.aleksejkocergin.kotlinapp.retrofit

import android.text.TextUtils
import okhttp3.Credentials
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ServiceGenerator {

    val API_BASE_URL: String = "https://gateway.watsonplatform.net/language-translator/api/"

    val okHttpClient = OkHttpClient.Builder()

    val builder = Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())

    var retrofit = builder.build()

    val httpLoggingInterceptor = HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY)

    fun <S> createService(serviceClass: Class<S>) : S {
        return createService(serviceClass, null, null)
    }

    fun <S> createService(serviceClass: Class<S>, username: String?, password: String?) : S {
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            val authToken = Credentials.basic(username!!, password!!)

            return createService(serviceClass, authToken)
        }
        return createService(serviceClass, null)
    }

    fun <S> createService(serviceClass: Class<S>, authToken: String?) : S {
        if (!TextUtils.isEmpty(authToken)) {
            val interceptor = AuthenticationInterceptor(authToken!!)
            if (!okHttpClient.interceptors().contains(interceptor)) {
                okHttpClient.addInterceptor(interceptor)
                        .addInterceptor(httpLoggingInterceptor)
                builder.client(okHttpClient.build())
                retrofit = builder.build()
            }
        }
        return retrofit.create(serviceClass)
    }
}