package com.example.aleksejkocergin.kotlinapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.example.aleksejkocergin.kotlinapp.R
import com.example.aleksejkocergin.kotlinapp.ui.views.IHistory

class FragmentHistory : MvpAppCompatFragment(), IHistory {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_history, container, false)

        return view
    }
}