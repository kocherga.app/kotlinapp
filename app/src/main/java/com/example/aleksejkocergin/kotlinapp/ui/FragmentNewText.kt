package com.example.aleksejkocergin.kotlinapp.ui

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.aleksejkocergin.kotlinapp.R
import com.example.aleksejkocergin.kotlinapp.presenters.NewTextPresenter
import com.example.aleksejkocergin.kotlinapp.ui.views.INewText
import kotlinx.android.synthetic.main.fragment_new_text.view.*

class FragmentNewText : MvpAppCompatFragment(), INewText {

    @InjectPresenter
    lateinit var newTextPresenter: NewTextPresenter

    private var alertDialog: AlertDialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?) : View? {
        val view = inflater.inflate(R.layout.fragment_new_text, container, false)

        view.fab.setOnClickListener {
            if (view.addEditText.text.toString().isNotBlank()) {
                newTextPresenter.determineLanguage(view.addEditText.text.toString())
            }
        }
        return view
    }

    override fun showMessage(message: String) {
        alertDialog(message)
    }

    override fun hideMessage() {
        alertDialog?.setOnDismissListener {  }
        alertDialog?.cancel()
    }

    override fun showProgress() {
        view!!.progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        view!!.progressBar.visibility = View.GONE
    }

    private fun alertDialog(message: String) {
        alertDialog = AlertDialog.Builder(activity)
                .setTitle("Language")
                .setMessage(message)
                .setOnDismissListener { newTextPresenter.hideDialog() }
                .show()
    }

    override fun onDestroy() {
        super.onDestroy()
        hideMessage()
    }
}