package com.example.aleksejkocergin.kotlinapp.retrofit

import com.example.aleksejkocergin.kotlinapp.models.Result
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface WatsonApiService {

    @Headers("Content-Type: text/plain")
    @POST("v3/identify?version=2018-05-01")
    fun getLanguage(@Body data: String) : Single<Result>
}