package com.example.aleksejkocergin.kotlinapp.ui.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.*

@StateStrategyType(SkipStrategy::class)
interface IMain : MvpView {

    fun showNewTextFragment()

    fun showHistoryFragment()
}